#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

## SHOPT ##
shopt -s autocd # change to named directory
shopt -s cdspell # auto correct cd misspellings
shopt -s cmdhist # save multi-line commands in history as single line
shopt -s dotglob
shopt -s histappend # do not overwrite history
shopt -s checkwinsize # checks term size when bash regains control

# ignore case when TAB completion
bind "set completion-ignore-case on"

# Use the up and down arrow keys for finding a command in history
# (you can write some initial letters of the command first).
bind '"\e[A":history-search-backward'
bind '"\e[B":history-search-forward'

export TERM="xterm-256color"
export HISTCONTROL=ignoredups:erasedups
export EDITOR=nano
export ANDROID_SDK_ROOT=$HOME/Android/Sdk

# Setup ccache
if command -v ccache &> /dev/null; then
  export USE_CCACHE=1
  export CCACHE_DIR=/mnt/source/.ccache
  export CCACHE_EXEC=$(which ccache)
fi

## PATH ##
if [ -d "$HOME/.bin" ]; then
  export PATH="$HOME/.bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ]; then
  export PATH="$HOME/.local/bin:$PATH"
fi

## OTHER RANDOM VARIABLES ##
if [ -z "$XDG_CONFIG_HOME" ]; then
  export XDG_CONFIG_HOME="$HOME/.config"
fi

if [ -z "$XDG_DATA_HOME" ]; then
  export XDG_DATA_HOME="$HOME/.local/share"
fi

if [ -z "$XDG_CACHE_HOME" ]; then
  export XDG_CACHE_HOME="$HOME/.cache"
fi

## Archive Exctraction ##
ex() {
  if [ -f "$1" ]; then
    case "$1" in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "$1 is not a valid file"
  fi
}

# navigation
up() {
  local d=""
  local limit="$1"

  if [ -z "$limit"] || [ "$limit" -le 0 ]; then
    limit=1
  fi

  for ((i=1;i<=limit;i++)); do
    d="../$d"
  done

  if ! cd "$d"; then
    echo "Couldn't go up $limit dirs."
  fi
}

function rcp() {
    rsync -rahP --modify-window=1 "$@"
}

function rmv() {
    rsync -rahP --modify-window=1 --prune-empty-dirs --remove-sent-files "$@"
}

# alias for cd to always set -P
alias cd='cd -P'

# common aliases
alias q='exit'
alias c='clear'
alias h='history'
alias t='time'

# alias chmod commands
alias mx='chmod a+x'
alias aw='chmod a+w'
alias 000='chmod -R 000'
alias 644='chmod -R 644'
alias 666='chmod -R 666'
alias 755='chmod -R 755'
alias 777='chmod -R 777'

# git aliases
alias g='git'
alias ga='git add'
alias gaa='git add -A'
alias gb='git branch'
alias gc='git checkout'
alias gs='git status'
alias gcl='git clone'
alias gst='git stash'
alias gl='git log'
alias gcm='git commit -m'
alias gcp="git cherry-pick"
alias gf="git fetch"
alias gr="git remote"
alias gra="git remote add"
alias gcan='git commit --amend --no-edit'
alias gd='git diff'
alias gp='git push'
alias grv='git remote -v'


# Changing ls to eza
alias ls='eza -al --color=always --group-directories-first' # my preferred listing
alias la='eza -a --color=always --group-directories-first'  # all files and dirs
alias ll='eza -l --color=always --group-directories-first'  # long format
alias lt='eza -aT --color=always --group-directories-first' # tree listing
alias l.='eza -a | egrep "^\."'

# pacman
alias pacsyu='sudo pacman -Syu'   # update only standard pkgs
alias pacsyyu='sudo pacman -Syyu' # Refresh pkglist & update standard pkgs

# get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

# get error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# restart qtile
alias qrs="qtile cmd-obj -o cmd -f restart"

alias hg='history | grep '
alias p='ps aux | grep '
alias topcpu='ps -eo pcpu,pid,user,args | sort -k 1 -r | head -10'
alias f='find . | grep '


alias diskspace='du -S | sort -n -r | more'
alias folders='find . -maxdepth 1 -type d -print0 | xargs -0 du -sk | sort -rn'

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# let root run our aliases
sa() {
    local func="${BASH_ALIASES[$1]}"
    shift

    echo sudo "$func" "$@"
}

### SETTING THE STARSHIP PROMPT ###
eval "$(starship init bash)"
