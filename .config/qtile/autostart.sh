#!/usr/bin/env bash

if [[ $(systemd-detect-virt) != "none" ]]; then
  if xrandr | grep "1920x1080"; then
    xrandr -s 1920x1080
  fi
fi

# restore wallpaper
#nitrogen --restore &
xwallpaper --stretch "$HOME/.local/share/wallpapers/arch_linux_purple.png" &

## Auto-start programs ##
lxsession &
picom &

killall conky
sleep 2
conky -c "$HOME/.config/conky/conky.conf"
